﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class inventoryScript : MonoBehaviour
{

    public int inventoryLimit = 10;
    public List<GameObject> inventory = new List<GameObject>();

    private int currentWeapon = 0;
    [HideInInspector]
    public GameObject selectedObject;
    [HideInInspector]
    public GameObject[] weaponList;
    private int weaponAmount = 4;
    private int selection = 0; //default value of


    public bool hg = false;      //Does player own handgun?
    public bool sg = false;  //Does player own Shotgun?
    public bool rifle = false; //Does player own rifle?

    // Update is called once per frame
    void Start()
    {
    }

    public void addObject(GameObject add)
    {
        if (inventory.Count < inventoryLimit)
        {
            GameObject addObject = Instantiate(add) as GameObject;

            inventory.Add(addObject);
        }
    }


    //Select the object in the inventory array.  If Bool up == true, then select up, if bool up == false, then go down.
    public void selectObject(bool up)
    {


        if (up == true)
        {
            if (selection >= inventoryLimit)  //Checker to verify we won't go above list length
            {
                selection = 0;  //Flips to beginning value of list
            }
            else  //If selection is less, add one to selection
            {
                selection++;
            }
        }

        else
        {
            if (selection <= 0)  //Checker to verify we won't go below list length
            {
                selection = (inventoryLimit - 1);  //Flips to end value of list
            }
            else
            {
                selection--;
            }
        }

        selectedObject = inventory[selection];
        // return selectedObject;
    }

    //Uses the currently selected object.
    public void useObject(GameObject use)
    {

        inventory.Remove(use);
        //Do something to activate the object
        activate(use);
        selectObject(true);
    }
    //Drops the currently selected object in front of player (can be picked up again)
    public void dropObject(GameObject drop)
    {
        Object.Instantiate(drop);
        inventory.Remove(drop);
        selectObject(true);
    }
    //Swaps to the weapon input in the Array (1 = fist, 2 = pistol, 3 = Shotgun, 4 = Rifle)
    public void swapWeapon(int number)
    {
        number -= 1;
        if (weaponList[(number)])
            //Setting all weapons' gameobjects inactive
            weaponList[0].gameObject.SetActive(false);
        weaponList[1].gameObject.SetActive(false);
        weaponList[2].gameObject.SetActive(false);
        weaponList[3].gameObject.SetActive(false);


        if (number == 2 && hg == true)
        {
            weaponList[1].gameObject.SetActive(true);
        }
        if (number == 3 && sg == true)
        {
            weaponList[2].gameObject.SetActive(true);
        }
        if (number == 4 && rifle == true)
        {
            weaponList[3].gameObject.SetActive(true);
        }
        if (number == 1)
        {
            weaponList[0].gameObject.SetActive(true);
        }
    }

    public void pickupWeapon(GameObject weapon)
    {
        if (weapon.transform.name == "handgun")
        {
            hg = true;
            Debug.Log("Picked up hangdun!");
        }
        if (weapon.transform.name == "shotgun")
        {
            sg = true;
            Debug.Log("Picked up shotgun!");
        }
        if (weapon.transform.name == "rifle")
        {
            rifle = true;
            Debug.Log("Picked up rifle!");
        }
    }

    private void activate(GameObject act)
    {
        if (act.name == "flare pickup")
        {
            Object flare = Instantiate(Resources.Load("Flare"));
        }
        else if (act.name == "ammoBox")
        {
            this.transform.GetComponent<weaponRifleScript>().rifleBullets += 20;
        }
        else if (act.name == "ammoBoxSG")
        {
            this.transform.GetComponent<weaponShotgunScript>().sgShells += 8;
        }
        else if (act.name == "ammoBoxHG")
        {
            this.transform.GetComponent<weaponHandgunScript>().hgBullets += 10;
        }
        else if (act.name == "CasettePlayer")
        {
            Debug.Log("Change Music?");
            //Actually Change music
        }
        else if (act.name == "Pills")
        {
            Debug.Log("Didn't your mother tell you not to eat strange pills?");
            //Kill player
        }
        else if (act.name == "Score")
        {
            Debug.Log("Add Score!");
        }
        else if (act.name == "Taser")
        {
            Debug.Log("Bzzzzt");
        }
        else if (act.name == "Flashlight")
        {
            Debug.Log("Awww... the batteries' dead!");
        }
        else if (act.name == "Battery")
        {

        }



    }
}
