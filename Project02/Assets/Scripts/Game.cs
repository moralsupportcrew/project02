﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Game 
{
  public int lives;
  public Vector3 playerPos;
  public float score;

  public Game()
  {
    lives = GameObject.Find("SingletonPlayerInformation").GetComponent<SingletonPlayerInformation>().currentLives;
    playerPos = GameObject.FindGameObjectWithTag("Player").transform.position;//GameObject.Find("Player").GetComponent<Transform>().Position;
    score = GameObject.Find("SingletonPlayerInformation").GetComponent<SingletonPlayerInformation>().currentScore;
  }
	
}
