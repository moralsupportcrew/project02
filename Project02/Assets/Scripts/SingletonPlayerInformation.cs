﻿using UnityEngine;
using System.Collections;

public class SingletonPlayerInformation : MonoBehaviour
{
  public static SingletonPlayerInformation Instance { get; private set; }
  [HideInInspector]
  public int currentLives = 3;
  [HideInInspector]
  public float currentScore = 50000;


  void Awake()
  {
    if (Instance != null && Instance != this)
    {
      Destroy(this.gameObject);

    }
    Instance = this;
    DontDestroyOnLoad(this.gameObject);
  }

  public void Death()
  {
    currentLives--;
  }
  // Use this for initialization
  void Start()
  {

  }

  // Update is called once per frame
  void Update()
  {
    currentScore--;
  }
}
