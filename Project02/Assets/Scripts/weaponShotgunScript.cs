﻿using UnityEngine;
using System.Collections;

public class weaponShotgunScript : MonoBehaviour {

    public int fireRate;
    public int numPellets = 7;
    public int damage;
    public int sgShells = 8;
    public float variance = 1.0f;
    public int range = 15;
    public const int sgMax = 25;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	for (int i = 0; i < 30; i++)
        {
            //Based off of a code in Java found at http://answers.unity3d.com/questions/504904/shotgun-raycast.html
            Vector3 offset = transform.up * Random.Range(0.0F, variance);
            offset = Quaternion.AngleAxis(Random.Range(0.0F, 360.0F), transform.forward) * offset;
            Vector3 hit = transform.forward * range + offset;
        }
	}
}
