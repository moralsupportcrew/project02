﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class UIScript : MonoBehaviour
{
  public Image life1;
  public Image life2;
  public Image life3;
  public Image noLife2;
  public Image noLife3;
  public Text ammoRemaining;
  public GameObject playerInfo;
  private SingletonPlayerInformation sPI;
  public float lives;

  // Use this for initialization
  void Start()
  {
    playerInfo = GameObject.Find("SingletonPlayerInformation");
    sPI = playerInfo.GetComponent<SingletonPlayerInformation>();
    life1.enabled = true;
    life2.enabled = true;
    life3.enabled = true;
    noLife2.enabled = false;
    noLife3.enabled = false;
    lives = sPI.currentLives;
  }

  // Update is called once per frame
  void Update()
  {
    lives = sPI.currentLives;
    if(lives == 2)
    {
      noLife3.enabled = true;
      life3.enabled = false;
    }
    if(lives == 1)
    {
      noLife3.enabled = true;
      life3.enabled = false;
      noLife2.enabled = true;
      life2.enabled = false;
    }
  }
}