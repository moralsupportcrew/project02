﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;


public class MenuScript : MonoBehaviour {

  public string currentScene;
  public string scene;

  // Use this for initialization
  void Start()
  {

  }

  // Update is called once per frame
  void Update()
  {
    //finds current scene
  }

  void OnTriggerEnter(Collider other)
  {
    if (other.name == "Player")
      SceneManager.LoadScene(scene);
  }

  public void Next(string nScene)
  {
    SceneManager.LoadScene(nScene);
  }

  public void Resume()
  {
    //sets current scene
  }

  public void _Quit()
  {
    Application.Quit();
  }



}
