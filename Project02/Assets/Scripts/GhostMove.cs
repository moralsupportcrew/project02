﻿using UnityEngine;
using System.Collections;

public class GhostMove : MonoBehaviour
{
    public float ghostSpeed;
    public GameObject targetPos;

    // Update is called once per frame
    void Update()
    {
        Seek();
    }

    void Seek()
    {
        Vector3 direction = targetPos.transform.position - transform.position;
        direction.Normalize();
        direction = direction * ghostSpeed * Time.deltaTime;
        transform.Translate(direction, Space.World);
    }
}
