﻿using UnityEngine;
using System.Collections;

public class SingletonAudioSource : MonoBehaviour
{
  public static SingletonAudioSource Instance { get; private set; }
  public AudioClip backgroundMusic;
  public AudioSource backgroundMusicS; 

  void Awake()
  {
    if (Instance != null && Instance != this)
    {
      Destroy(this.gameObject);

    }
    Instance = this;
    DontDestroyOnLoad(this.gameObject);
    Music();
  }

  public void Music()
  {
    backgroundMusicS.clip = backgroundMusic;
    backgroundMusicS.Play();
    backgroundMusicS.loop = true;
  }
  // Use this for initialization
  void Start()
  {

  }

  // Update is called once per frame
  void Update()
  {

  }
}
