﻿using UnityEngine;
using System.Collections;

public class pickUpScript : MonoBehaviour {
    GameObject player;

    // Use this for initialization
    void Start () {
        player = GameObject.Find("player"); ;
    }
	
	// Update is called once per frame
	void Update () {
	}

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "pickup")
        {

            if (player.GetComponent<inventoryScript>().inventory.Count < player.GetComponent<inventoryScript>().inventoryLimit)
            {
                player.GetComponent<inventoryScript>().addObject(col.gameObject);
                Destroy(col.gameObject);
            }
            else
            {
                Debug.Log("Player has full inventory!");
            }
        }

        if (col.gameObject.tag == "weapon")
        {
            player.GetComponent<inventoryScript>().pickupWeapon(col.gameObject);
            Destroy(col.gameObject);
            Debug.Log("Picking up:" + col.gameObject.name);
        }
    }
}
