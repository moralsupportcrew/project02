﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

//To Aiden and Bowen, if you're looking through here the most common thing being broken is because you need to setup in the
//Input Manager a button called "Crouch", i personally use "left shift" for the button, do whatever you feel is better though

public class physicsMovement: MonoBehaviour {

    Rigidbody myBody;

    public float walkSpeed = 10.0f;
    public float crouchSpeed = 4.0f;
    public GameObject pInfo;
    
    //public float speed = 10.0f;
    [Tooltip("This dictates how quickly you can change your speed, a higher number let's you"
        + " get up to max speed quicker")]
    public float maxVelocityChange = 10.0f;
    public bool canJump = true;
    public float jumpHeight = 2.0f;
    [Tooltip("If this is checked true when on keyboard and you have two inputs pressed" +
        " it limits your speed in the diagonal directions")]
    public bool limitDiagonalSpeed = false;
    

    private bool grounded = false;
    private float speed;
    
    void Awake()
    {
        GetComponent<Rigidbody>().freezeRotation = true;
        GetComponent<Rigidbody>().useGravity = false;
    }

    void Start()
    {
        myBody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        if (grounded)
        {
             /*if (Input.GetKey(KeyCode.ESC))
             {
               SceneManager.LoadScene(SaveScene);
               
             }*/
            //check if we're crouching and change speed modifier
            speed = Input.GetButton("Crouch") ? crouchSpeed : walkSpeed;

            // Calculate how fast we should be moving
            //absolute valueing the vert component because we can't move backwards for some reason
            Vector3 targetVelocity = new Vector3(Input.GetAxis("Horizontal"), 0, Mathf.Abs(Input.GetAxis("Vertical")));
            targetVelocity = transform.TransformDirection(targetVelocity);
            if (limitDiagonalSpeed && targetVelocity.magnitude > 1)
                targetVelocity.Normalize();
            targetVelocity *= speed;

            // Apply a force that attempts to reach our target velocity
            Vector3 velocity = myBody.velocity;
            Vector3 velocityChange = (targetVelocity - velocity);
            velocityChange.x = Mathf.Clamp(velocityChange.x, -maxVelocityChange, maxVelocityChange);
            velocityChange.z = Mathf.Clamp(velocityChange.z, -maxVelocityChange, maxVelocityChange);
            velocityChange.y = 0;
            myBody.AddForce(velocityChange, ForceMode.VelocityChange);

            // Jump
            if (canJump && Input.GetButton("Jump"))
            {
                myBody.velocity = new Vector3(velocity.x, CalculateJumpVerticalSpeed(), velocity.z);
            }
        }

        // We apply gravity manually for more tuning control
        //myBody.AddForce(new Vector3(0, -gravity * myBody.mass, 0));
        myBody.AddForce(Physics.gravity * myBody.mass);

        grounded = false;
    }

    /*void OnCollisionEnter(Collision col)
    {
      if (col.gameObject.tag == "Ghost")
      {
        if (gameObject.Find("SingletonPlayerInformation").getComponent<SingletonPlayerInformation>().currentLives == 1)
          SceneManager.LoadScene("GameOver");
        else
          gameObject.Find("SingletonPlayerInformation").getComponent<SingletonPlayerInformation>().currentLives--;
      }

    }*/

    //if hitting the ground, grounding should be true
    void OnCollisionStay()
    {
        grounded = true;
    }

    float CalculateJumpVerticalSpeed()
    {
        // From the jump height and gravity we deduce the upwards speed 
        // for the character to reach at the apex.
        return Mathf.Sqrt(2 * jumpHeight * Physics.gravity.magnitude);
    }
}
